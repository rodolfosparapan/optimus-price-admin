<?
require_once 'HtmlForm.php';

class TextArea implements HtmlForm{
    
    private $sHtml;
    
    public function __construct($aData, $sValue='') {
        
        $this->sHtml = "<textarea class='form-control' name='".$aData['name']."' id='".$aData['name']."'>".valueToShow($sValue)."</textarea>";
    }
    
    public function getHtml(){
        
        return $this->sHtml;
    }
}