<?
require_once 'HtmlForm.php';

class Radio implements HtmlForm{
    
    private $sHtml;
    
    public function __construct($aData, $sValueSelected='') {
        
        foreach($aData['options'] as $sValue => $sTitle){
        
            $bChecked = ($sValueSelected == $sValue || ($sValue == 'S' && $sValueSelected == '')? 'checked' : '');
            $this->sHtml .= "<input type='radio' name='".$aData['name']."' value='$sValue' $bChecked /> $sTitle <br/>";
        }
    }
    
    public function getHtml(){
        
        return $this->sHtml;
    }
}