<?
require_once 'HtmlForm.php';

class InputText implements HtmlForm{
    
    private $sHtml;
    
    public function __construct($aData, $sValue='') {
        
        $this->sHtml = "<input type='text' class='form-control' name='".$aData['name']."' id='".$aData['name']."' value='".valueToShow($sValue)."' />";
    }
    
    public function getHtml(){
        
        return $this->sHtml;
    }
}