<?
require_once 'HtmlForm.php';

class Label implements HtmlForm{
    
    private $sHtml;
    
    public function __construct($sValue) {
        
        $this->sHtml = "<p class='text-primary' style='font-size:16px; padding-top:5px'>$sValue</p>";
    }
    
    public function getHtml(){
        
        return $this->sHtml;
    }
}