<?
require_once 'DropList.php';
require_once 'InputHidden.php';
require_once 'InputPassword.php';
require_once 'InputText.php';
require_once 'Radio.php';
require_once 'Label.php';
require_once 'TextArea.php';

interface HtmlForm{
    
   public function getHtml();
}