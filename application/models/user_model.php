<?

class User_model extends MY_Model{
    
    private $user_data;
    
    public function auth($sUser, $sPassword){
        
        $sql = "SELECT * FROM users WHERE login=? AND password=? AND active=?";
        $query = $this->db->query($sql, array($sUser, $sPassword, 'S'));
        
        if($query->num_rows() > 0){
            
            $this->user_data = $query->row_array();
            return true;
        }
        
        return false;
    }
    
    public function getUserData(){
        
        return $this->user_data;
    }
}