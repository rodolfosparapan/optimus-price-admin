<?php

class Form_model extends MY_Model{
    
    protected $bSuccess;
    protected $sMessage;
    protected $insert_id;

    public function commit($sTable, $sKey, $aData){
        
        if($aData[$sKey] > 0){
            
            $this->bSuccess = $this->update($sTable, $aData, $sKey);
            $this->sMessage = ($this->bSuccess ? lang('update_succes') : lang('update_error'));
            
        }else{

            $this->bSuccess = $this->insert($sTable, $aData);
            $this->sMessage = ($this->bSuccess ? lang('insert_succes') : lang('insert_error'));
        }
    }
    
    public function update($sTable, $aData, $sKey){
        
        $this->db->where($sKey, $aData[$sKey]);
        return $this->db->update($sTable, $this->prepareToDB($aData)); 
    }
    
    public function insert($sTable, $aData){
        
        $bSuccess = $this->db->insert($sTable, $this->prepareToDB($aData));
        $this->insert_id = mysql_insert_id();
        
        return $bSuccess;
    }
    
    
}