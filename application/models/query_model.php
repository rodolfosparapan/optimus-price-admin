<?php

class Query_model extends MY_Model{
    
    private $iTotalRows;
    
    public function getValues($sSearch, $sTable, $aSearchFields, $aFields, $sDefaultFilter, $sPerPage, $iCurrentPage){
        
        $sqlCount = "SELECT " . implode(",", array_keys($aFields)) . " FROM $sTable " . $this->getWhere($sSearch, $aSearchFields, $sDefaultFilter);
        $this->iTotalRows = mysql_num_rows(mysql_query($sqlCount));
        
        $sStartLimit = ($iCurrentPage * $sPerPage) - $sPerPage;
        $sql = "SELECT " . implode(",", array_keys($aFields)) . " FROM $sTable " . $this->getWhere($sSearch, $aSearchFields, $sDefaultFilter) . " LIMIT $sStartLimit, $sPerPage";
        return $this->getArray($sql, $aFields);
    }
    
    public function getTotalRows(){
        
        return $this->iTotalRows;
    }
    
    public function getFormValues($sTable, $sFieldKey, $iKeyValue){
        
        $sql = "SELECT * FROM $sTable WHERE $sFieldKey=?";
        $query = $this->db->query($sql, array($iKeyValue));
        
        if($query->num_rows() > 0){
        
            $aResult = $query->result();
            return $aResult[0];
        }
        
        return '';
    }
}