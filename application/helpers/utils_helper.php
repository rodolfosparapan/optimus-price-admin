<?

function setDateBR($sDate){
    
    if(empty($sDate)){
        
        return '';
    }
    
    $aDate = explode("-", $sDate);
    
    return $aDate[2] . "/" . $aDate[1] . "/" . $aDate[0] . (isset($aDate[3]) ? $aDate[3] : '');
}

function setMysqlDate($sDate){
    
    $aDate = explode("/", $sDate);
    
    return $aDate[2] . "-" . $aDate[1] . "-" . $aDate[0];
}

function valueToShow($sValue){
    
    $sValueRet = html_escape($sValue);
    
    $sDatetimeFormat = DateTime::createFromFormat('Y-m-d G:i:s', $sValue);
    $sDateFormat = DateTime::createFromFormat('Y-m-d', $sValue);
    if ($sDatetimeFormat !== FALSE || $sDateFormat !== FALSE) {
        
        $sValueRet = setDateBR($sValue);
    }
    
    return $sValueRet;
}