<?

function getPagination($iCurrentPage, $iPerPage, $iTotalRows){
    
    $iTotalPages = ceil($iTotalRows / $iPerPage);
    
    if($iTotalPages > 1){
        
        $sHtmlPage = '<b>' . $iTotalRows . '</b> Resultados <div class="dataTables_paginate paging_bs_normal"><ul class="pagination">';
        $start = (ceil($iCurrentPage / 5) * 5) - 4;
        
        if($iCurrentPage > 5){

            $sHtmlPage .= '<li class="prev"><a href="javascript:search('.($start-1).')"><span class="fa fa-angle-left"></span>&nbsp;Anterior</a></li>';
        }
        
        $iPage = $start;
        for($i=1; $i<=5; $i++){
        
            if($iPage == $iCurrentPage){
                
                $sHtmlPage .= "<li class='active'><a href='javascript:search($iPage)'>$iPage</a></li>";
                
            }else{
                
                $sHtmlPage .= "<li><a href='javascript:search($iPage)'>$iPage</a></li>";
            }
            
            if($i == $iTotalPages) break;
            
            $iPage++;
        }
        
        if($iCurrentPage < $iTotalPages && $iTotalPages > 5){

            $sHtmlPage .= '<li class="next"><a href="javascript:search('.$iPage.')">Próximo&nbsp;<span class="fa fa-angle-right"></span></a></li>';
        }
        
        $sHtmlPage .= "</ul></div>";
        
        return $sHtmlPage;
    }
    
    return '<div style="margin-top:40px"><b>' . $iTotalRows . '</b> '.($iTotalRows == 1 ? lagn('result') : lang('result').'s').'</div>';
}