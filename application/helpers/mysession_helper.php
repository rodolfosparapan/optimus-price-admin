<?

function setLoginSession(){

    $oCi = get_instance();
    
    $oCi->session->set_userdata('auth', true);
    $oCi->session->set_userdata('user_data', $oCi->user_model->getUserData());
    $oCi->session->set_userdata('log_at', date('H:i:s'));
}

function setLogoutSession(){

    $oCi = get_instance();
    
    $oCi->session->set_userdata('auth', false);
    $oCi->session->set_userdata('user_data', array());
    $oCi->session->set_userdata('log_at', null);
}
