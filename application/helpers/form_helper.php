<?

function getFormElement($aData, $sValue){
  
    get_instance()->load->_interface('form/HtmlForm');
    
    switch($aData['type']){
        
        case 'text':
            $oForm = new InputText($aData, $sValue); break;
        
        case 'password':
            $oForm = new InputPassword($aData, $sValue); break;
        
        case 'label':
            $oForm = new Label($sValue); break;
        
        case 'radio':
            $oForm = new Radio($aData, $sValue); break;
        
        case 'textarea':
            $oForm = new TextArea($aData, $sValue); break;
    }
    
    return $oForm->getHtml();
} 