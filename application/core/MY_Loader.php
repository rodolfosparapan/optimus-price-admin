<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Loader extends CI_Loader{
    
    public function template($sViewPath, $aArgs=array()){
        
        $this->view('includes/header');
        $this->view($sViewPath, $aArgs);
        $this->view('includes/footer');
    }
    
    public function _interface($strInterfaceName){
        
        require_once APPPATH . '/libraries/' . $strInterfaceName . '.php';
    }
    
    
    public function query_template($oController, $aQueryConfig){
        
        if($oController->input->is_ajax_request()){
            
            $iCurrentPage = $oController->input->post('page');
            $aQueryConfig['current_page'] = ($iCurrentPage > 1 ? $iCurrentPage : 1);
            $aQueryConfig['query'] = $oController->input->post('search');
            
            return $oController->load->view('templates/query/inner_query_result.php', $aQueryConfig);
        }
        
        $oController->load->template('templates/query/query_template', $aQueryConfig);
    }
}