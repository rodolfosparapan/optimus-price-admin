<?php

class MY_Model extends CI_Model{
    
    public function MY_Model(){
        
        $this->load->database();
    }
    
    protected function getArray($sql, $aFields = array()){
        
        $stmt = mysql_query($sql);
        
        $aRet = array();
        while($rs = mysql_fetch_object($stmt)){
            
            if(count($aFields) > 0){
                
                $aPartial = array();
                foreach(array_keys($aFields) as $sField){
                    $aPartial[$sField] = $rs->$sField;
                }
                $aRet[] = $aPartial;
                
            }else{
                
                $aRet[] = $rs;
            }
        }
        
        return $aRet;
    }
    
    
    protected function getWhere($sSearch, $aSearchFields, $sDefaultFilter){
        
        $aWhere = array();
        foreach($aSearchFields as $sField){
            
            $aParts = explode(' ', trim($sSearch));
            foreach($aParts as $sPart){
             
                $aWhere[] = "$sField LIKE'%$sPart%'";
            }
        }
        
        return "WHERE (" . implode(' OR ', $aWhere). ")" . (!empty($sDefaultFilter) ? " AND $sDefaultFilter" : "");
    }
    
    protected function prepareToDB($aData){
        
        $aRet = array();
        foreach($aData as $sField => $sValue){
            
            $sCleanField = $sField;
            $aFd = explode('|', $sField);
            if(count($aFd) > 1){
                $sCleanField = $aFd[1];
            }
            
            if(substr($sCleanField, 0, 2) == 'dt'){
            
                $aRet[$sCleanField] = setMysqlDate($sValue);
                
            }else if(str_replace("price", "", $sCleanField) != $sCleanField){
                
                $aRet[$sCleanField] = str_replace(',', '.', $sValue);
                
            }else{
                
                $aRet[$sCleanField] = $sValue;
            }
        }
        
        return $aRet;
    }
}