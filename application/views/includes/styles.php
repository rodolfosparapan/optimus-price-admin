
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS -->
<link href="<?=base_url()?>public/js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/jquery.gritter/css/jquery.gritter.css" />

<link rel="stylesheet" href="<?=base_url()?>public/fonts/font-awesome-4/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="../../assets/js/html5shiv.js"></script>
  <script src="../../assets/js/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/jquery.nanoscroller/nanoscroller.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/jquery.easypiechart/jquery.easy-pie-chart.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/bootstrap.switch/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/jquery.select2/select2.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/js/bootstrap.slider/css/slider.css" />
<link href="<?=base_url()?>public/css/style.css" rel="stylesheet" />
<link href="<?=base_url()?>public/css/flag-icon.min.css" rel="stylesheet" />