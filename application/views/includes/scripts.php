
<script src="<?=base_url()?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
<script src="<?=base_url()?>public/js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.nestable/jquery.nestable.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/bootstrap.switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.select2/select2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>public/js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/behaviour/general.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.validator/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.validator/location/messages_pt_BR.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
    });
</script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?=base_url()?>public/js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/jquery.flot/jquery.flot.labels.js"></script>