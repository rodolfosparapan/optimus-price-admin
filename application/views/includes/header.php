<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.png">

        <title>OptimusPrice <?=(!empty($page_name) ? " - $page_name" : "")?> </title>

        <? include APPPATH . 'views/includes/styles.php' ?>

    </head>

    <body>

        <!-- Fixed navbar -->
        <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="fa fa-gear"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url() ?>"><span>Optimus Price</span></a>
                </div>

                <? $aMenu = $this->global_model->getMenu(); ?>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <? foreach ($aMenu as $oMenu): ?>
                            <li class="active"><a href="<?= base_url() . $oMenu->url ?>"><?= $oMenu->menu ?></a></li>
                        <? endforeach; ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right user-nav">
                        <li class="dropdown profile_menu">
                            <a style="margin-top:6px" href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!--<img alt="Avatar" src="<?= base_url() ?>public/images/avatar2.jpg" />--> 
                                Rodolfo Sparapan <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>user/account"><?=lang('my_account')?></a></li>
                                <li class="divider"></li>
                                <li><a href="<?=base_url()?>user/logout"><?=lang('logout')?></a></li>
                            </ul>
                        </li>
                        <li style="padding:5px 0px 0 10px">
                            <a style="padding:0" href="<?=base_url()?>user/language/pt-br"><span class="label label-default"><span class="flag-icon flag-icon-br"></span> BR</span><br/></a>
                            <a style="padding:0" href="<?=base_url()?>user/language/english"><span class="label label-default"><span class="flag-icon flag-icon-gb"></span> EN</span></a>
                        </li>
                    </ul>			

                </div>
            </div>
        </div>