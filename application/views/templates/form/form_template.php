<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.png">

        <title>OptimusPrice - <?=$page_name?> </title>

        <? include APPPATH . 'views/includes/styles.php' ?>

    </head>

    <body>

        <? include APPPATH . 'views/includes/header.php' ?>

        <div id="cl-wrapper">

            <div class="container-fluid">

                <div class="cl-mcont">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="block-flat">
                                <div class="header">	
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3><?=$title?>
                                                <?if(!isset($values->{$key_id})):?>
                                                <small class="text-primary"> ( <?=lang('new')?> )</small>
                                                <?else:?>
                                                    <small class="text-primary"> ( Cod. <?=$values->{$key_id}?> ) </small>
                                                <?endif;?>
                                            </h3>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <button id="save-form" class="btn btn-primary"><?=lang('save')?></button>
                                            <?if(!isset($back)):?>
                                                <a href='<?=base_url() . $this->router->fetch_class()?>/query' class="btn btn-default"><?=lang('back')?></a>
                                            <?endif;?>
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <form style="border-radius: 0px;" id="form-template" action="#" class="form-horizontal group-border-dashed">
                                        <input type="hidden" name="<?=$key_id?>" value="<?(isset($values->{$key_id}) ? $values->{$key_id} : 0)?>">
                                        <?foreach($fields as $aDataForm):?>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?=$aDataForm['title']?></label>
                                                <div class="col-sm-<?=(isset($aDataForm['size']) ? $aDataForm['size']: 6)?>">
                                                    <?=getFormElement($aDataForm, (isset($values->{$aDataForm['name']}) ? $values->{$aDataForm['name']} : ''));?>
                                                </div>
                                            </div>
                                        <?endforeach;?>
                                        
                                    </form>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>

            </div>

        </div>

        <? include APPPATH . 'views/includes/scripts.php'; ?>

        <script type="text/javascript">
            
            $('#save-form').click(function(){
                submitForm();
            });
    
            $(document).keypress(function(e){
                if(e.keyCode === 13){
                    submitForm();
                }
            });
    
            function submitForm(){
                $.ajax({
                    method: "POST",
                    url: "<?=base_url().$this->router->fetch_class()?>/save",
                    data: {form_data: $('#form-template').serialize()}
                })
                .done(function( msg ) {
                  alert(msg);
                });
            }
            
        </script>
        
    </body>
</html>