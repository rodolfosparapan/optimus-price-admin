

        <div id="cl-wrapper">
           
            <div class="container-fluid">

                <div class="cl-mcont">
                    
                    <h3><?=$page_name?> 
                        <a href="<?=base_url().$this->router->fetch_class()?>/form">
                            <small class="text-primary">( <!--<i class="fa fa-plus"></i>--> <?=lang('new')?>)</small>
                        </a>
                    </h3>
                    
                    <div class="row">
                        <div class="col-md-5 col-xs-9">
                            <div class="input-group">
                                <?=lang('search')?>: <b><?=implode(", ", $search_fields)?></b>
                                <input class="form-control" type="search" name="query" id="query"/>
                                <span class="input-group-btn">
                                    <div style="margin-top:21px"><button id="query-bottom" type="button" class="btn btn-primary fa fa-search fa-2x"></button></div>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-7 col-xs-9 text-right">
                            <div id="inner-pagination"></div>
                        </div>
                    </div>
                    
                    <div id="innerQueryResult" style="margin-top:10px"></div>

                </div>

            </div> 

        </div>

        <?include APPPATH . 'views/includes/scripts.php';?>
        
        <script type="text/javascript">
            
            function search(){
                $.ajax({
                    method: 'POST',
                    url: '<?=base_url().$this->router->fetch_class().'/query'?>',
                    data: {
                        search:$('#query').val(),
                        page: arguments[0]
                    } 
                    }).done(function(sHtmlReturn) {
                        aHtml = sHtmlReturn.split('{PAGINATION}');
                        $('#innerQueryResult').html(aHtml[0]);
                        $('#inner-pagination').html(aHtml[1]);
                });
            }
            
            $('#query-bottom').click(function(){
                search();
            });
            
            $('#query').keypress(function(e){
                if(e.which === 13){
                    search();
                }
            });
            
        </script>
