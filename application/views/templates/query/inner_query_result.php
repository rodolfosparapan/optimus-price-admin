<?
$aRowValues = $this->query_model->getValues($query, $table, array_keys($search_fields), $fields, $default_filter, $per_page, $current_page);
$iTotalRows = $this->query_model->getTotalRows();
?>

<?if($iTotalRows > 0):?>

    <div class="block-flat">
        <table class="no-border blue hover">
                <thead class="no-border">
                        <tr>
                            <?foreach($fields as $sField => $sTitle):?>
                                <?if($sField == 'active') continue;?>
                                <th><?=$sTitle?></th>
                            <?endforeach;?>
                        </tr>
                </thead>

                <tbody class="no-border-x no-border-y">

                    <?foreach($aRowValues as $aColValues):?>
                        <tr onclick="document.location.href='<?=base_url().$this->router->fetch_class().'/form'?>/<?=$aColValues[$field_key]?>'">
                            <?foreach($aColValues as $sField => $sValue):?>
                            <td>
                                <?
                                if($sField == "active"){
                                    continue;
                                }else if(array_key_exists($sField, $call_function)){
                                    echo call_user_func($call_function[$sField], $sValue);
                                }else{
                                    echo $sValue;
                                }
                                ?>
                            </td>
                            <?endforeach;?>
                        </tr>
                    <?endforeach;?>
                </tbody>
        </table>
    </div>

    {PAGINATION}

    <?=getPagination($current_page, $per_page, $iTotalRows)?>

<?else:?>
    
    <h3 style="margin-top:10%" class="text-center text-primary"><?=lang('no_results')?></h3>
    
<?endif;?>
