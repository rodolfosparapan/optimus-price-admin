<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.png">

        <title><?= APP_NAME ?></title>

        <? include APPPATH . 'views/includes/styles.php' ?>
        
        <style type="text/css">
            body{
                background-color: #f5f5f5 !important;
            }
        </style>

    </head>

    <body>

        <div id="cl-wrapper" class="login-container">

            <div class="middle-login">
                <div class="block-flat">
                    <div class="header">							
                        <h3 class="text-center"><img class="logo-img" src="<?=base_url()?>public/images/logo.png" alt="logo"/><?=APP_NAME?></h3>
                    </div>
                    <div>
                        <form id="loginForm" style="margin-bottom: 0px !important;" class="form-horizontal" action="user/auth" method="POST"> 
                            <div class="content">
                                <h4 class="title">Login</h4>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" placeholder="<?=lang('user')?>" name="username" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                            <input type="password" placeholder="<?=lang('password')?>" name="password" class="form-control" required minlength="6">
                                        </div>
                                    </div>
                                </div>
                                
                                <?if($this->session->flashdata('error_message') != ''):?>
                                    <p class="text-danger"><?=$this->session->flashdata('error_message')?></p>
                                <?endif;?>

                            </div>
                            <div class="foot">
                                <!--<button class="btn btn-default" data-dismiss="modal" type="button">Register</button>-->
                                <button class="btn btn-primary" data-dismiss="modal" type="submit">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--<div class="text-center out-links"><a href="#">&copy; 2015 <?=APP_NAME?></a></div>-->
            </div> 

        </div>

        <? include APPPATH . 'views/includes/scripts.php'; ?>

        <script type="text/javascript">
            $("#loginForm").validate();
        </script>
        
    </body>
</html>
