<?
$lang['yes'] = 'Yes';
$lang['no']  = 'No';
$lang['save']  = 'Save';
$lang['back']  = 'Back';

$lang['search'] = 'Search By';
$lang['result'] = 'Result';
$lang['new'] = 'New';
$lang['no_results'] = 'No Result was found!';