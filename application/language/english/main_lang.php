<?
$lang['my_account'] = 'My Account';
$lang['logout'] = 'Logout';
$lang['welcome_to'] = 'Welcome to';

$lang['name'] = 'Name';
$lang['user'] = 'User';
$lang['password'] = 'Password';
$lang['password_confirm'] = 'Password Confirm';

$lang['query_sales']    = 'Sales';
$lang['sale']           = 'Sale';
$lang['sales']          = 'Sales';
$lang['start_at']       = 'Start At';
$lang['end_at']         = 'End At';
$lang['cupom_code']     = 'Cupom Code';
$lang['description']    = 'Description';
$lang['active']         = 'Active';

$lang['product'] = 'Product';
$lang['products'] = 'Products';
$lang['price'] = 'Price';
$lang['description'] = 'Description';

$lang['user_management'] = 'User Management';