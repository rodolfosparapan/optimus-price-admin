<?
$lang['yes'] = 'Yes';
$lang['no']  = 'No';
$lang['save']  = 'Salvar';
$lang['back']  = 'Voltar';

$lang['search'] = 'Pesquise por';
$lang['result'] = 'Resultado';
$lang['new'] = 'Novo';
$lang['no_results'] = 'Nenhum resultado foi encontrado';