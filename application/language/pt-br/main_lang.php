<?php

$lang['my_account'] = 'Meu Cadastro';
$lang['logout'] = 'Sair';
$lang['welcome_to'] = 'Bem Vindo ao';

$lang['name'] = 'Name';
$lang['user'] = 'Usuário';
$lang['password'] = 'Senha';
$lang['password_confirm'] = 'Confirme sua Senha';

$lang['query_sales']    = 'Consulta de Promoções';
$lang['sale']           = 'Promoções';
$lang['sales']          = 'Promoção';
$lang['start_at']       = 'Dt.Início';
$lang['end_at']         = 'Dt.Término';
$lang['cupom_code']     = 'Cupom de Desconto';
$lang['description']    = 'Descrição';
$lang['active']         = 'Ativo';

$lang['product'] = 'Produto';
$lang['products'] = 'Produtos';
$lang['price'] = 'Preço';
$lang['description'] = 'Descrição';

$lang['user_management'] = 'Gerenciar Usuário';