<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends CI_Controller {
    
    public function query(){
    
        $aQueryConfig = array(
            'field_key'     => 'sale_id',
            'page_name'     => lang('sales'),
            'table'         => 'vw_sales',
            'fields'        => array(
                'sale_id'       => 'Id',
                'company_name'  => lang('company'),
                'sale'          => lang('sale'),
                'dt_start'      => lang('start_at'),
                'dt_end'        => lang('end_at'),
                'cupom_code'    => lang('cupom_code'),
                'description'   => lang('description'),
                'active'        => lang('active')
            ),
            'search_fields'  => array(
                'sale_id'    => 'Id', 
                'sale'       => lang('sale'),
                'cupom_code' => lang('cupom_code')
            ),
            'call_function' => array(
                'dt_start'  => 'setDateBR',
                'dt_end'    => 'setDateBR',
            ),
            'default_filter'        => "active='S'",
            'per_page'      => 10
        );
        
        return $this->load->query_template($this, $aQueryConfig);
    }
    
    
    public function form($iSaleId=0){
        
        $aForm = array(
            'title' => lang('sales'),
            'key_id' => 'sale_id',
            'values' => $this->query_model->getFormValues("vw_sales", "sale_id", $iSaleId),
            'fields' => array(
                array(
                    'type' => 'label',
                    'title' => lang('company'),
                    'name' => 'company_name'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('sale'),
                    'name' => 'sale'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('start_at'),
                    'name' => 'dt_start',
                    'size' => '2'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('end_at'),
                    'name' => 'dt_end',
                    'size' => '2'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('cupom_code'),
                    'name' => 'cupom_code',
                    'size' => '2'
                ),
                array(
                    'type' => 'textarea',
                    'title' => lang('description'),
                    'name' => 'description'
                ),
                array(
                    'type' => 'radio',
                    'title' => lang('active'),
                    'name' => 'active',
                    'options' => array(
                        'S' => lang('yes'),
                        'N' => lang('no'),
                    )
                )
            )
        );

        $this->load->template('templates/form/form_template', $aForm);
    }
}