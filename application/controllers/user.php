<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function login(){
        
        if($this->session->userdata('auth')){
        
            return $this->dashboard();
        }
        
        $this->load->view('user/login');
    }
    
    public function auth(){
     
        $this->load->model('user_model');
        $bAuth = $this->user_model->auth($this->input->post('username'), $this->input->post('password'));
        
        if($bAuth){
            
            $this->load->helper('mysession');
            setLoginSession();
            redirect('/');
        }
        
        $this->session->set_flashdata('error_message', lang('invalid'));
        redirect('/');
    }
    
    public function dashboard(){
        
        $this->load->template('dashboard');
    }
    
    public function logout(){
        
        $this->load->helper('mysession');
        setLogoutSession();
        
        redirect('/');
    }
    
    public function account(){
        
        $aUser = $this->session->userdata('user_data');
        $iUserId = $aUser['user_id'];
        
        $aForm = array(
            'title' => lang('user_management'),
            'key_id' => 'user_id',
            'values' => $this->query_model->getFormValues("users", "user_id", $iUserId),
            'fields' => array(
                array(
                    'type' => 'text',
                    'title' => lang('name'),
                    'name' => 'name'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('user'),
                    'name' => 'login'
                ),
                array(
                    'type' => 'password',
                    'title' => lang('password'),
                    'name' => 'password',
                    'size' => '2'
                ),
                array(
                    'type' => 'password',
                    'title' => lang('password_confirm'),
                    'name' => 'password_confirm',
                    'size' => '2'
                ),
                array(
                    'type' => 'radio',
                    'title' => lang('active'),
                    'name' => 'active',
                    'options' => array(
                        'S' => lang('yes'),
                        'N' => lang('no'),
                    )
                )
            ),
            'back' => false
        );

        $this->load->template('templates/form/form_template', $aForm);
        //$this->load->template('user/account');
    }
    
    public function language($sLanguage){
        
        $this->session->set_userdata('site_lang', $sLanguage);
        redirect('/');
    }
}