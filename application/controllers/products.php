<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {
    
    public function query(){
        
        $aQueryConfig = array(
            'field_key'     => 'product_id',
            'page_name'     => lang('product'),
            'table'         => 'products',
            'fields'        => array(
                'product_id'    => 'Id',
                'product_name'  => lang('product'),
                'price'         => lang('price'),
                'description'   => lang('description'),
            ),
            'search_fields'  => array(
                'product_id'    => 'Id',
                'product_name'  => lang('product'),
                'description'   => lang('description'),
                'price'         => lang('price'),
            ),
            'default_filter'        => "active='S'",
            'per_page'      => 10
        );
        
        return $this->load->query_template($this, $aQueryConfig);
    }
    
    
    public function form($iProductId=0){
        
        $aForm = array(
            'title' => lang('products'),
            'key_id' => 'product_id',
            'values' => $this->query_model->getFormValues("products", "product_id", $iProductId),
            'fields' => array(
                array(
                    'type' => 'text',
                    'title' => lang('product'),
                    'name' => 'product_name'
                ),
                array(
                    'type' => 'text',
                    'title' => lang('price'),
                    'name' => 'price',
                    'size' => '2'
                ),
                array(
                    'type' => 'textarea',
                    'title' => lang('description'),
                    'name' => 'description'
                ),
                array(
                    'type' => 'radio',
                    'title' => lang('active'),
                    'name' => 'products|active',
                    'options' => array(
                        'S' => lang('yes'),
                        'N' => lang('no'),
                    )
                )
            )
        );

        $this->load->template('templates/form/form_template', $aForm);
    }
    
    
    public function save(){
        
        $aData = array();
        parse_str($this->input->post('form_data'), $aData);
        
        $this->form_model->commit('producs', 'product_id', $aData);
        
        return json_encode(array('bSuccess' => $this->bSuccess, 'message' => $this->sMessage, 'insert_id' => $this->insert_id));
    }
    
}