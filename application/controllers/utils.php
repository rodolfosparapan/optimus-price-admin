<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utils extends CI_Controller {

    function make_base(){

        $this->load->library('VpxMigration');

        // All Tables:
        $this->vpxmigration->generate();

        //Single Table:
        // $this->vpxmigration->generate('table');
    }
}