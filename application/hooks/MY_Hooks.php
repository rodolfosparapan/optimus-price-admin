<?

class Hooks{
    
    private $oCi;
    
    public function __construct() {
        
        $this->oCi = get_instance();
    }
    
    public function check_auth(){
        
        $aRestrictMethods = array(
            'dashboard', 'account'
        );
        
        $aRestrictControllers = array(
            'promocoes'
        );
        
        if((in_array($this->oCi->router->fetch_method(), $aRestrictMethods) || 
            in_array($this->oCi->router->fetch_class(), $aRestrictControllers)) && 
            $this->oCi->session->userdata('auth') != true){
            
            redirect('/');
        }
    }
    
    function changeLanguage() {
        
        $sDefaultLang = $this->oCi->config->item('language');
        $sSessionLang = $this->oCi->session->userdata('site_lang');
        $sCurrentLanguage = ($sSessionLang != '' ? $sSessionLang : $sDefaultLang);
        
        $this->oCi->config->set_item('language', $sCurrentLanguage);
        
        $aLanguages = array('main', 'templates', 'messages');
        foreach($aLanguages as $sLanguage){
            
            $this->oCi->lang->load($sLanguage);
        }
    }
}