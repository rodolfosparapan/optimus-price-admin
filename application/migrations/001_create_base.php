<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_base extends CI_Migration {

	public function up() {

		## Create Table companies
		$this->dbforge->add_field("`company_id` int(10) NOT NULL auto_increment");
		$this->dbforge->add_key("company_id",true);
		$this->dbforge->add_field("`company_name` varchar(45) NOT NULL ");
		$this->dbforge->add_field("`active` enum('S','N') NOT NULL DEFAULT 'S' ");
		$this->dbforge->create_table("companies", TRUE);
		$this->db->query('ALTER TABLE  `companies` ENGINE = InnoDB');
		
                ## Create Table sales
		$this->dbforge->add_field("`sale_id` int(10) NOT NULL ");
		$this->dbforge->add_key("sale_id",true);
		$this->dbforge->add_field("`company_id` int(10) NOT NULL ");
		$this->dbforge->add_field("`sale` varchar(45) NOT NULL ");
		$this->dbforge->add_field("`dt_start` date NULL ");
		$this->dbforge->add_field("`dt_end` date NULL ");
		$this->dbforge->add_field("`description` text NULL ");
		$this->dbforge->add_field("`cupom_code` varchar(45) NULL ");
		$this->dbforge->add_field("`active` enum('S','N') NOT NULL DEFAULT 'S' ");
		$this->dbforge->create_table("sales", TRUE);
		$this->db->query('ALTER TABLE  `sales` ENGINE = InnoDB');
		
                ## Create Table system_menu
		$this->dbforge->add_field("`menu_id` int(10) NOT NULL auto_increment");
		$this->dbforge->add_key("menu_id",true);
		$this->dbforge->add_field("`menu` varchar(45) NOT NULL ");
		$this->dbforge->add_field("`position` int(10) NOT NULL ");
		$this->dbforge->add_field("`url` varchar(100) NOT NULL ");
		$this->dbforge->add_field("`ativo` enum('S','N') NOT NULL DEFAULT 'S' ");
		$this->dbforge->add_field("`language` varchar(20) NOT NULL ");
		$this->dbforge->create_table("system_menu", TRUE);
		$this->db->query('ALTER TABLE  `system_menu` ENGINE = InnoDB');
		
                ## Create Table users
		$this->dbforge->add_field("`user_id` int(10) NOT NULL auto_increment");
		$this->dbforge->add_key("user_id",true);
		$this->dbforge->add_field("`name` varchar(45) NOT NULL ");
		$this->dbforge->add_field("`login` varchar(45) NOT NULL ");
		$this->dbforge->add_field("`password` varchar(45) NULL ");
		$this->dbforge->add_field("`active` enum('S','N') NOT NULL DEFAULT 'S' ");
		$this->dbforge->create_table("users", TRUE);
		$this->db->query('ALTER TABLE  `users` ENGINE = InnoDB');
	}

	public function down()	{
		### Drop table companies ##
		$this->dbforge->drop_table("companies", TRUE);
		### Drop table sales ##
		$this->dbforge->drop_table("sales", TRUE);
		### Drop table system_menu ##
		$this->dbforge->drop_table("system_menu", TRUE);
		### Drop table users ##
		$this->dbforge->drop_table("users", TRUE);
        }
}